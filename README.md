# exercices-math


## Lancer SageMath localement utilisant Docker

Pour ne pas devoir installer localement les paquets requis.

Ne marche que sur Linux.

```sh
./docker/launchnotebook
```

La version Docker supporte la conversion à Pdf.


## Lancer SageMath directement de votre fureteur web

Pourrait ne pas marcher si Mybinder est surchargé.

https://mybinder.org/v2/gl/tlvu%2Fexercices-math/main?labpath=sec2/v2/cible-cercle-sec2-exercice-math-probabilite-algebre.ipynb
