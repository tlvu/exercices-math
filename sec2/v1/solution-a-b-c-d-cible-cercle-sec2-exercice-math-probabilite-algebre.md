[//]: # "Reference https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/doc/user/markdown.md#math"

[//]: # "https://katex.org/docs/supported.html"

## Solution a), b), c), d)

### Formules

   * Circonférence cercle: $`2 \pi r`$, $`\pi`$: constante (3.1416), r: rayon

   * Aire cercle: $`\pi r^2`$

   * Aire losange: $`\dfrac{D \times d}{2}`$, D: grande diagonale, d: petite diagonale


#### Résoudre x et y en utilisant circonférence $`C_3 = 16 \pi`$ et aire $`\lozenge = 72`$.  Nous avons 2 variables, il faut minimalement 2 équations.

#### Équation 1:

   $`C_3 = 16 \pi = 2 \pi r_3`$

   $`r_3 = x + 2y`$

#### Résoudre pour trouver l'équation 1:

   $`16 \pi = 2 \pi r_3`$

   $`16 = 2 r_3`$

   $`8 = r_3`$

   $`8 = x + 2y \longrightarrow`$ équation 1


#### Équation 2:

   $`aire \lozenge = 72 = \dfrac{D \times d}{2} = \dfrac{2r_2 \times 2r_2}{2}`$

   $`r_2 = x + y`$

#### Résoudre pour trouver l'équation 2:

   $`72 = \dfrac{2r_2 \times 2r_2}{2}`$

   $`144 = 2r_2 \times 2r_2`$

   $`144 = 4(r_2)^2`$

   $`36 = (r_2)^2`$

   $`6^2 = (r_2)^2`$

   $`6 = r_2`$

   $`6 = x + y \longrightarrow`$ équation 2


#### Utilisant les 2 équations pour trouver x et y:

   $`6 = x + y \longrightarrow 6 - x = y`$

   $`8 = x + 2y \longrightarrow 8 = x + 2(6 - x)`$

   $`8 = x + 12 - 2x`$

   $`8 = -x + 12`$

   $`-4 = -x`$

   $`x = 4`$

   $`y = 6 - x = 6 - 4 = 2`$


### a) Quelle est la propabilité que la balle tombe dans la zone $`C_1`$?

   $`P(C_1) = \dfrac{Aire C_1}{\text{Aire totale cible}} = \dfrac{Aire C_1}{Aire C_3} = \dfrac{\pi (r_1)^2}{\pi (r_3)^2} = \dfrac{\pi x^2}{\pi (x + 2y)^2}`$ = ?


Aire $`C_1 = \pi(r_1)^2 = \pi x^2 = \pi 4^2 = 16 \pi`$

Aire $`C_3 = \pi(r_3)^2 = \pi (x+2y)^2 = \pi (4 + 2 \cdot 2)^2 = \pi (4 + 4)^2 = \pi 8^2 = 64 \pi`$

$`P(C_1) = \dfrac{Aire C_1}{Aire C_3} = \dfrac{16 \pi}{64 \pi} = \dfrac{16}{64} = \dfrac{1}{4} = 25 \%`$


### b) $`P(\lozenge - C_1)`$ = Propabilité que la balle tombe dans zone losange mais à l'extérieur de la zone $`C_1`$ = ?

$`P(\lozenge - C_1) = P(C_\lozenge) - P(C_1)`$

$`P(\lozenge) = \dfrac{Aire \lozenge}{Aire C_3} = \dfrac{72}{64 \pi} = \dfrac{9}{8 \pi} \approx 35.81 \%`$

$`P(\lozenge - C_1) = P(C_\lozenge) - P(C_1) = \dfrac{9}{8 \pi} - \dfrac{1}{4} = \dfrac{9 - 2 \pi}{8 \pi} \approx 10.81 \%`$


### c) $`P(C_2 - \lozenge)`$ = ?

$`P(C_2 - \lozenge) = P(C_2) - P(\lozenge)`$

$`P(C_2) = \dfrac{Aire C_2}{Aire C_3}`$

$`Aire C_2 = \pi (r_2)^2 = \pi (x + y)^2 = \pi (4 + 2)^2 = \pi 6^2 = 36 \pi`$

$`P(C_2) = \dfrac{Aire C_2}{Aire C_3} = \dfrac{36 \pi}{64 \pi}= \dfrac{36}{64} = \dfrac{9}{16} = 56.25 \%`$

$`P(C_2 - \lozenge) = P(C_2) - P(\lozenge) = \dfrac{9}{16} - \dfrac{9}{8 \pi} = \dfrac{9 \pi - 18}{16 \pi} \approx 20.44 \%`$


### d) $`P(C_3 - C_2)`$ = ?

$`P(C_3 - C_2) = P(C_3) - P(C_2)`$

$`P(C_3) = \dfrac{Aire C_3}{Aire C_3} = 1 = 100 \%`$

$`P(C_3 - C_2) = P(C_3) - P(C_2) = 1 - \dfrac{9}{16} = \dfrac{16 - 9}{16} = \dfrac{7}{16}= 43.75 \%`$

### Vérification:

$`P(C_1) + P(\lozenge - C_1) + P(C_2 - \lozenge) + P(C_3 - C_2) \stackrel{?}{=} 100 \%`$

$`25 \% + 10.81 \% + 20.44 \% + 43.75 \% = 100 \%`$

$`P(\lozenge - C_1) + P(C_2 - \lozenge) \stackrel{?}{=} P(C_2) - P(C_1)`$

$`0.1081 + 0.2044 \stackrel{?}{=} 0.5625 - 0.25`$

$`0.3125 = 0.3125`$
