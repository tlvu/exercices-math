[//]: # "Reference https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/doc/user/markdown.md#math"

[//]: # "https://katex.org/docs/supported.html"

## Solution e), f), g)

Si tu lances la balle 2 fois:

### e) Quelle est la probabilité que la balle tombe dans la zone "$`C_1`$" ou "$`C_3 - C_2`$" et ensuite encore dans la zone "$`C_1`$" ou "$`C_3 - C_2`$", sans être la même zone que le premier lancé, l'ordre n'a pas d'importance?

  $`P(C_1, C_3 - C_2) + P(C_3 - C_2, C_1) = P(C_1) \times P(C_3 - C_2) + P(C_3 - C_2) \times P(C_1) = 2 \times P(C_1) \times P(C_3 - C_2)`$

  $` = 2 (\dfrac{1}{4}) (\dfrac{7}{16}) = (\dfrac{1}{2}) (\dfrac{7}{16}) = \dfrac{7}{32} \approx 21.88 \%`$


### f) Quelle est la probabilité que les 2 lancés tombent dans la même zone?

  $`P(C_1, C_1) + P(\lozenge - C_1, \lozenge - C_1) + P(C_2 - \lozenge, C_2 - \lozenge) + P(C_3 - C_2, C_3 - C_2) = ?`$

  $` = (P(C_1))^2 + (P(\lozenge - C_1))^2 + (P(C_2 - \lozenge))^2 + (P(C_3 - C_2))^2`$

  $` = (\dfrac{1}{4})^2 + (\dfrac{9 - 2 \pi}{8 \pi})^2 + (\dfrac{9 \pi - 18}{16 \pi})^2 + (\dfrac{7}{16})^2 = \dfrac{1}{16} + \dfrac{(9 - 2 \pi)(9 - 2 \pi)}{64 \pi^2} + \dfrac{(9 \pi - 18)(9 \pi - 18)}{256 \pi^2} + \dfrac{49}{256}`$

  $` = \dfrac{1}{16} + \dfrac{81 - 18\pi - 18\pi + 4\pi^2}{64 \pi^2} + \dfrac{81 \pi^2 - 162 \pi - 162 \pi + 324}{256 \pi^2} + \dfrac{49}{256}`$

  $` = \dfrac{1}{16} + \dfrac{81 - 36 \pi + 4\pi^2}{64 \pi^2} + \dfrac{81 \pi^2 - 324 \pi + 324}{256 \pi^2} + \dfrac{49}{256}`$

  $` = \dfrac{1}{2^4} + \dfrac{81 - 36 \pi + 4\pi^2}{2^6 \pi^2} + \dfrac{81 \pi^2 - 324 \pi + 324}{2^8 \pi^2} + \dfrac{49}{2^8}`$

  $` = \dfrac{2^4 \pi^2 \cdot 1}{2^4 \pi^2 \cdot 2^4} + \dfrac{2^2 \cdot (81 - 36 \pi + 4\pi^2)}{2^2 \cdot 2^6 \pi^2} + \dfrac{81 \pi^2 - 324 \pi + 324}{2^8 \pi^2} + \dfrac{\pi^2 \cdot 49}{\pi^2 \cdot 2^8}`$

  $` = \dfrac{2^4 \pi^2}{2^8 \pi^2} + \dfrac{2^2 \cdot 3^4 - 2^2 \cdot 2^2 3^2 \pi + 2^2 \cdot 2^2\pi^2}{2^8 \pi^2} + \dfrac{3^4 \pi^2 - 2^2 3^4 \pi + 2^2 3^4}{2^8 \pi^2} + \dfrac{7^2 \pi^2}{2^8 \pi^2}`$

  $` = \dfrac{(2^4 + 2^4 + 3^4 + 7^2) \pi^2 + (- 2^4 3^2 - 2^2 3^4) \pi + (2^2 3^4 + 2^2 3^4)}{2^8 \pi^2}`$

  $` = \dfrac{162 \pi^2 - 2^2 3^2 (2^2 + 3^2) \pi + 2^3 3^4}{2^8 \pi^2}`$

  $` = \dfrac{2 \cdot 3^4 \pi^2 - 2^2 3^2 (2^2 + 3^2) \pi + 2^3 3^4}{2^8 \pi^2}`$

  $` = \dfrac{2 (3^4 \pi^2 - 2 \cdot 3^2 (2^2 + 3^2) \pi + 2^2 3^4)}{2^8 \pi^2}`$

  $` = \dfrac{3^4 \pi^2 - 2 \cdot 3^2 (2^2 + 3^2) \pi + 2^2 3^4}{2^7 \pi^2} \approx \dfrac{388.305}{1263.309} \approx 30.74 \%`$

#### Vérification

  $`P(C_1))^2 + (P(\lozenge - C_1))^2 + (P(C_2 - \lozenge))^2 + (P(C_3 - C_2))^2`$

  $` = 0.25^2 + 0.1081^2 + 0.2044^2 + 0.4375^2 \stackrel{?}{\approx} 0.3074`$

  $` = 0.0625 + 0.0117 + 0.0418 + 0.1914 = 0.3074`$


### g) Quelle est la probabilité que les 2 lancés tombent dans les zones différentes?

  P(2 lancés tombent dans les zones différentes)

  = 100% - P(2 lancés tombent dans la même zone)

  = 1 - réponse de la question prédédente

  $` = \dfrac{2^7 \pi^2 - (3^4 \pi^2 - 2 \cdot 3^2 (2^2 + 3^2) \pi + 2^2 3^4)}{2^7 \pi^2}`$

  $` = \dfrac{(2^7 - 3^4) \pi^2 + 2 \cdot 3^2 (2^2 + 3^2) \pi - 2^2 3^4}{2^7 \pi^2}`$

  $` = \dfrac{47 \pi^2 + 2 \cdot 3^2 (2^2 + 3^2) \pi - 2^2 3^4}{2^7 \pi^2} \approx \dfrac{875.004}{1263.309} \approx 69.26 \%`$
