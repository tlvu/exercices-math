[//]: # "Reference https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/doc/user/markdown.md#math"

[//]: # "https://katex.org/docs/supported.html"

![Cible circulaire](cible-cercle-sec2-exercice-math-probabilite-algebre.jpg "Cible circulaire"){width=50%}

Circonférence $`C_3 = 16 \pi = 2 \pi r = 2 \pi r_3 = 2 \pi (x + 2y)`$ unités.

Aire $`\lozenge = 72 = \dfrac{D \times d}{2} = \dfrac{2r_2 \times 2r_2}{2} = \dfrac{2(x + y) \times 2(x + y)}{2}`$ unités carrés.

Tu lances une balle au hasard sur une cible circulaire ci-haut.  La circonférence de $`C_3`$ est $`16 \pi`$ unités et l'aire du losange est 72 unités carrés.  Trouves les réponses suivantes en fraction pour garder la précision et en pourcentage arrondi au centière près.

a) Quelle est la propabilité que la balle tombe dans la zone $`C_1`$?

   $`P(C_1) = \dfrac{Aire C_1}{\text{Aire totale cible}} = \dfrac{Aire C_1}{Aire C_3} = \dfrac{\pi (r_1)^2}{\pi (r_3)^2} = \dfrac{\pi x^2}{\pi (x + 2y)^2}`$ = ?

b) $`P(\lozenge - C_1)`$ = Propabilité que la balle tombe dans zone losange mais à l'extérieur de la zone $`C_1`$ = ?

c) $`P(C_2 - \lozenge)`$ = ?

d) $`P(C_3 - C_2)`$ = ?

Si tu lances la balle 2 fois:

e) Quelle est la probabilité que la balle tombe dans la zone "$`C_1`$" ou "$`C_3 - C_2`$" et ensuite encore dans la zone "$`C_1`$" ou "$`C_3 - C_2`$", sans être la même zone que le premier lancé, l'ordre n'a pas d'importance?

Pour f) et g), pas besoin de donner la réponse précise en fraction, juste le pourcentage c'est assez.

f) Quelle est la probabilité que les 2 lancés tombent dans la même zone?

g) Quelle est la probabilité que les 2 lancés tombent dans les zones différentes?
